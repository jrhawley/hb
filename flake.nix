{
  description = "A CLI for HomeBank";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-parts.url = "github:hercules-ci/flake-parts";
    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };
  };

  outputs =
    { self
    , nixpkgs
    , crane
    , flake-parts
    , fenix
    , advisory-db
    , ...
    }@inputs:

    flake-parts.lib.mkFlake { inherit inputs; } {
      flake = { };

      systems = [
        "x86_64-linux"
        "x86_64-darwin"
      ];

      perSystem = { config, pkgs, system, ... }: {
        devShells.default = import ./shell.nix {
          inherit pkgs;
          craneLib = (crane.mkLib pkgs);
          fenix = fenix.packages.${system}.stable.withComponents [
            "cargo"
            "clippy"
            "llvm-tools"
            "rust-src"
            "rustc"
          ];
        };

        formatter = pkgs.alejandra;

        packages.default = import ./default.nix {
          inherit pkgs;
          craneLib = (crane.mkLib pkgs);
        };
      };
    };
    # in
    # {
    #   checks = {
    #     inherit hb;

    #     hb-clippy = craneLib.cargoClippy (
    #       commonArgs // {
    #         inherit cargoArtifacts pname;
    #         cargoClippyExtraArgs = "--all-targets -- --deny warnings";
    #       }
    #     );

    #     hb-doc = craneLib.cargoDoc (commonArgs // {
    #       inherit cargoArtifacts;
    #     });

    #     # Check formatting
    #     hb-fmt = craneLib.cargoFmt {
    #       inherit src pname;
    #     };

    #     # Audit dependencies
    #     hb-audit = craneLib.cargoAudit {
    #       inherit src advisory-db pname;
    #     };

    #     # Audit licenses
    #     hb-deny = craneLib.cargoDeny {
    #       inherit src pname;
    #     };

    #     # Run tests with cargo-nextest
    #     # Consider setting `doCheck = false` on `hb` if you do not want
    #     # the tests to run twice
    #     hb-nextest = craneLib.cargoNextest (commonArgs // {
    #       inherit cargoArtifacts;
    #       partitions = 1;
    #       partitionType = "count";
    #     });

    #   };


    #   packages = {
    #     hb-llvm-coverage = craneLibLLvmTools.cargoLlvmCov (
    #       commonArgs // {
    #         inherit cargoArtifacts;
    #       }
    #     );
    #   };
    # });
}

