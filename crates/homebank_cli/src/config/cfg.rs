//! The configuration struct and related items

use super::{
    parse::{expand_tilde, file_to_string},
    ConfigError,
};
use crate::cli::CliOpts;
use clap::crate_name;
use dirs_next::config_dir;
use serde::Deserialize;
use std::{
    fs::canonicalize,
    path::{Path, PathBuf},
};

/// The `hb` configuration.
#[derive(Debug, Deserialize, PartialEq)]
pub struct Config {
    /// Path to the HomeBank transactions file.
    ///
    /// This can be either an absolute or relative path.
    /// Relative paths are resolved relative to the configuration file itself (i.e. `.` refers to the directory containing the configuration file).
    path: PathBuf,
}

impl Config {
    /// Create a new `Config`
    pub fn new(path: &Path) -> Self {
        Config {
            path: path.to_path_buf(),
        }
    }

    // Retrieve the path to the HomeBank XHB file
    pub fn xhb_path(&self) -> &Path {
        &self.path
    }
}

impl TryFrom<&CliOpts> for Config {
    type Error = ConfigError;

    fn try_from(opts: &CliOpts) -> Result<Self, Self::Error> {
        // check that the config file exists
        if !opts.path.exists() {
            return Err(ConfigError::DoesNotExist(opts.path().to_path_buf()));
        } else if !opts.path.is_file() {
            // check that the config is a file
            return Err(ConfigError::NotAFile(opts.path().to_path_buf()));
        } else {
            // read the file and parse its contents
            let file_contents = match file_to_string(&opts.path) {
                Ok(s) => s,
                Err(_) => return Err(ConfigError::ParseError(opts.path().to_path_buf())),
            };

            // try to deserialize from its contents via toml
            Config::try_from((file_contents.as_str(), opts.path()))
        }
    }
}

impl TryFrom<(&str, &Path)> for Config {
    type Error = ConfigError;

    fn try_from((s, cfg_path): (&str, &Path)) -> Result<Self, Self::Error> {
        let mut cfg: Config = match toml::from_str(s) {
            Ok(cfg) => cfg,
            Err(_) => return Err(ConfigError::MissingHomeBankPath(cfg_path.to_path_buf())),
        };

        // if the path is tilded, fix it
        if let Some(d) = expand_tilde(cfg.xhb_path()) {
            cfg.path = d;
        }

        // resolve the relative path to the HomeBank XHB file, if needed
        if cfg.xhb_path().is_relative() {
            // we already know that `cfg_path` is a file, so we can unwrap it with certainty
            if let Ok(new_path) = canonicalize(cfg_path.parent().unwrap().join(cfg.xhb_path())) {
                cfg.path = new_path;
            } else {
                return Err(ConfigError::ParseError(cfg_path.to_path_buf()));
            }
        }

        // check that the HomeBank XHB file is a file
        if !cfg.xhb_path().is_file() {
            return Err(ConfigError::HomeBankFileNotAFile(
                cfg.xhb_path().to_path_buf(),
            ));
        }

        Ok(cfg)
    }
}

/// The default folder for the application's configuration
fn default_cfg_dir() -> PathBuf {
    let cfg_dir = match config_dir() {
        Some(d) => d,
        None => PathBuf::from("~/.config"),
    };

    cfg_dir.join(crate_name!())
}

/// The default configuration file
pub fn default_cfg_file() -> PathBuf {
    default_cfg_dir().join("config.toml")
}

#[cfg(test)]
mod tests {
    use super::*;
    use dirs_next::home_dir;

    #[test]
    #[cfg(target_os = "linux")]
    fn default_linux_config_dir() {
        let expected = home_dir().unwrap().join(".config/hb/");
        let observed = default_cfg_dir();

        assert_eq!(expected, observed);
    }

    #[test]
    #[cfg(target_os = "windows")]
    fn default_windows_config_dir() {
        let expected = home_dir().unwrap().join("AppData/Roaming/hb/");
        let observed = default_cfg_dir();

        assert_eq!(expected, observed);
    }

    #[test]
    #[cfg(target_os = "macos")]
    fn default_macos_config_dir() {
        use std::path::Path;

        let expected = home_dir().unwrap().join("Library/Application Support/hb/");
        let observed = default_cfg_dir();

        assert_eq!(expected, observed);
    }

    #[test]
    #[cfg(target_os = "linux")]
    fn default_linux_config_file() {
        let expected = home_dir().unwrap().join(".config/hb/config.toml");
        let observed = default_cfg_file();

        assert_eq!(expected, observed);
    }

    #[test]
    #[cfg(target_os = "windows")]
    fn default_windows_config_file() {
        let expected = home_dir().unwrap().join("AppData/Roaming/hb/config.toml");
        let observed = default_cfg_file();

        assert_eq!(expected, observed);
    }

    #[test]
    #[cfg(target_os = "macos")]
    fn default_macos_config_file() {
        use std::path::Path;

        let expected = home_dir()
            .unwrap()
            .join("Library/Application Support/hb/config.toml");
        let observed = default_cfg_file();

        assert_eq!(expected, observed);
    }

    #[track_caller]
    fn check_new(input: &Path, expected: Config) {
        let observed = Config::new(input);

        assert_eq!(expected, observed);
    }

    #[test]
    #[cfg(target_os = "linux")]
    fn new_absolute_paths_stay_absolute() {
        let input = Path::new("/etc/passwd");
        let expected = Config {
            path: PathBuf::from("/etc/passwd"),
        };

        check_new(input, expected);
    }

    #[test]
    fn new_existing() {
        let input = Path::new("Cargo.toml");
        let expected = Config {
            path: PathBuf::from("Cargo.toml"),
        };

        check_new(input, expected);
    }

    #[track_caller]
    fn check_try_from_cli(input: CliOpts, expected: Config) {
        let observed = Config::try_from(&input).unwrap();

        assert_eq!(expected, observed);
    }

    #[test]
    #[should_panic]
    fn try_from_directory_config() {
        let cli_opts = CliOpts {
            path: PathBuf::from("./src"),
            subcmd: None,
        };
        let expected = Config::new(Path::new("path"));

        check_try_from_cli(cli_opts, expected);
    }

    #[test]
    #[should_panic]
    fn try_from_nonexistent_config() {
        let cli_opts = CliOpts {
            path: PathBuf::from("path/to/nonexistent/directory/file.toml"),
            subcmd: None,
        };
        let expected = Config::new(Path::new(""));

        check_try_from_cli(cli_opts, expected)
    }

    #[test]
    #[cfg(target_os = "linux")]
    fn try_from_existing_config_absolute_existing_xhb() {
        let input = CliOpts::new(Path::new("tests/absolute_existing_linux.toml"), None);
        let expected = Config {
            path: PathBuf::from("/etc/passwd"),
        };

        check_try_from_cli(input, expected);
    }

    #[test]
    #[cfg(target_os = "linux")]
    #[should_panic]
    fn try_from_existing_config_relative_existing_xhb() {
        let input = CliOpts::new(Path::new("tests/relative_existing_linux.toml"), None);
        let expected = Config {
            path: PathBuf::from("/etc/passwd"),
        };

        check_try_from_cli(input, expected);
    }

    #[test]
    #[cfg(target_os = "linux")]
    #[should_panic]
    fn try_from_existing_config_absolute_missing_xhb() {
        let input = CliOpts::new(Path::new("tests/absolute_missing_linux.toml"), None);
        let expected = Config {
            path: PathBuf::from("/etc/passwd"),
        };

        check_try_from_cli(input, expected);
    }

    #[test]
    #[should_panic]
    fn try_from_str_without_path() {
        let input = "";
        Config::try_from((input, Path::new("/etc/passwd"))).unwrap();
    }

    #[test]
    #[cfg(target_os = "linux")]
    fn try_from_str_with_path() {
        let input = "path = '/etc/passwd'";
        // The `/etc/passwd` is just a stand in for the config file path.
        // This is only used for error messages or resolving relative links, so it doesn't matter for this test.
        let cfg_path = Path::new("/etc/passwd");
        let expected = Config::new(Path::new("/etc/passwd"));

        let observed = Config::try_from((input, cfg_path)).unwrap();

        assert_eq!(expected, observed);
    }

    #[test]
    fn relative_xhb_path_is_resolved() {
        let cfg_path = PathBuf::from("./tests/minimal.toml");
        let file_contents = file_to_string(&cfg_path).unwrap();

        // we need to canonicalize the paths so that the resolution is the same
        let expected_xhb_path =
            canonicalize(PathBuf::from("../homebank_db/tests/minimal.xhb")).unwrap();
        let expected = Config::new(&expected_xhb_path);
        let observed = Config::try_from((file_contents.as_str(), cfg_path.as_path())).unwrap();

        assert_eq!(expected, observed);
    }
}
