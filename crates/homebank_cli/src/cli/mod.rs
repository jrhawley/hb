//! CLI argument parsing and configuration

pub mod command;

pub use command::{CliOpts, SubCommand};
