//! A command line interface for [HomeBank](https://homebank.free.fr).
//!
//! The interface allows you to query the HomeBank database by using the [`homebank_db`] crate.

use anyhow::Context;
use clap::Parser;
use cli::{CliOpts, SubCommand};
use config::Config;
use homebank_db::{transaction::sum_transactions, HomeBankDb, Query, QueryType};

pub mod cli;
pub mod config;

/// Run the command line interface.
fn main() -> Result<(), anyhow::Error> {
    let cli_opts = CliOpts::parse();

    let cfg = Config::try_from(&cli_opts)?;
    let db = match HomeBankDb::try_from(cfg.xhb_path()) {
        Ok(db) => db,
        Err(e) => return Err(e).with_context(|| "Error parsing HomeBank file."),
    };

    match &cli_opts.subcommand() {
        Some(SubCommand::Query(q_opts)) => match q_opts.qtype() {
            QueryType::Transactions(query) => {
                let filt_transactions = query.exec(&db);

                println!("{:#?}", filt_transactions);
            }
            QueryType::Payees(query) => {
                let filt_payees = query.exec(&db);

                println!("{:#?}", filt_payees);
            }
            QueryType::Currencies(query) => {
                let filt_currencies = query.exec(&db);

                println!("{:#?}", filt_currencies);
            }
            QueryType::Categories(query) => {
                let filt_categories = query.exec(&db);

                for cat in filt_categories {
                    println!("{}", cat.full_name(&db));
                }
            }
            QueryType::Accounts(query) => {
                let filt_accounts = query.exec(&db);

                println!("{:#?}", filt_accounts);
            }
            QueryType::Groups(query) => {
                let filt_groups = query.exec(&db);

                println!("{:#?}", filt_groups);
            }
        },
        // QueryType::Templates(query) => {
        //     let filt_templates = query.exec(&db);

        //     println!("{:#?}", filt_templates);
        // }
        Some(SubCommand::Sum(query)) => {
            let filt_transactions = query.exec(&db);
            let sum = sum_transactions(&filt_transactions);
            println!("{sum:.2}");
        }
        Some(SubCommand::Budget(query)) => {
            let budgets = query.exec(&db);

            for budget in budgets {
                // extract correct string values or empty strings
                let subname = match budget.subname() {
                    Some(val) => val.to_string(),
                    None => String::new(),
                };

                let allotment = match budget.allotment() {
                    Some(val) => format!("{val:.2}"),
                    None => String::new(),
                };

                let progress = format!("{:.2}", budget.progress());

                println!("{}\t{}\t{}\t{}", budget.name, subname, allotment, progress);
            }
        }
        Some(SubCommand::Review(query)) => {
            let reviews = query.exec(&db);

            // print the values in a tab-separated format
            for review in reviews {
                if let Some(subcat_name) = review.subcat_name {
                    println!(
                        "{}\t{}\t{}\t{:.2}",
                        review.account_name,
                        review.cat_name,
                        subcat_name,
                        review.sum
                    );
                } else {
                    println!("{}\t{}\t\t{:.2}", review.account_name, review.cat_name, review.sum);
                }
            }
        }
        None => {}
    }

    Ok(())
}
