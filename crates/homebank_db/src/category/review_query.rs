//! Review the sums across each (sub)category in your HomeBank database.

use super::{FIRST_OF_NEXT_MONTH_STR, TODAY_FIRST_OF_MONTH_STR};
use crate::{transaction::{sum_transactions, transaction_date::HB_MIN_DATE}, HomeBankDb, Query, QueryTransactions};

use chrono::{Local, NaiveDate};
use clap::Parser;
use itertools::Itertools;
use regex::Regex;
use std::{fmt::Display, str::FromStr};

/// Query the budget in your HomeBank database.
#[derive(Debug, Parser)]
pub struct QueryReview {
    /// Consider the budget from the month including this date.
    #[clap(
        short = 'd',
        long = "date-from",
        default_value = &TODAY_FIRST_OF_MONTH_STR,
        parse(try_from_str = NaiveDate::from_str),
        value_name = "date"
    )]
    pub(crate) date_from: NaiveDate,

    /// Consider the budget from the month up to and excluding this date.
    #[clap(
        short = 'D',
        long = "date-to",
        default_value = &FIRST_OF_NEXT_MONTH_STR,
        parse(try_from_str = NaiveDate::from_str),
        value_name = "date"
    )]
    pub(crate) date_to: NaiveDate,

    /// Exclude any (sub)categories that have no transactions.
    #[clap(short = 'x')]
    pub(crate) exclude_none: bool,
}

#[derive(Debug, Clone)]
/// The sum of all [`Transaction`s][crate::transaction::transaction_struct::Transaction], for a given [`Category`][crate::category::category_struct::Category].
pub struct ReviewSummary {
    /// The [`Category`][crate::category::category_struct::Category] name.
    pub account_name: String,

    /// The [`Category`][crate::category::category_struct::Category] name.
    pub cat_name: String,

    /// The sub-[`Category`][crate::category::category_struct::Category] name, if there is one.
    pub subcat_name: Option<String>,

    /// The number of [`Transaction`s][crate::transaction::transaction_struct::Transaction] included in the summary.
    pub n_transactions: usize,

    /// The total sum of [`Transaction`s][crate::transaction::transaction_struct::Transaction] over the time span provided.
    pub sum: f32,
}

impl QueryReview {
    /// Create a new query for budgets
    pub fn new(date_from: NaiveDate, date_to: NaiveDate, exclude_none: bool) -> Self {
        Self {
            date_from,
            date_to,
            exclude_none,
        }
    }

    /// Retrieve the earliest date that the budget is including
    fn date_from(&self) -> &NaiveDate {
        &self.date_from
    }

    /// Retrieve the latest date that the budget is including
    fn date_to(&self) -> &NaiveDate {
        &self.date_to
    }

    /// Retrieve whether some
    fn excluded_none(&self) -> bool {
        self.exclude_none
    }
}

impl Default for QueryReview {
    fn default() -> Self {
        Self { date_from: *HB_MIN_DATE, date_to: Local::now().date_naive(), exclude_none: false }
    }
}

impl Display for QueryReview {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Query for QueryReview {
    type T = ReviewSummary;

    fn exec(&self, db: &HomeBankDb) -> Vec<Self::T> {
        let categories = db.categories().values();
        let accounts = db.accounts().values();

        let mut vals: Vec<ReviewSummary> = categories.cartesian_product(accounts)
            .map(|(cat, acct)| {
                // create a regex from the category name (match the name exactly to exclude subcategories)
                let cat_re = format!("^{}$", &cat.full_name(db));
                let cat_name_re = Regex::from_str(&cat_re).unwrap();
                let acct_re = format!("^{}$", &acct.name());
                let acct_name_re = Regex::from_str(&acct_re).unwrap();

                // get all the transactions for that category
                let transaction_query = QueryTransactions {
                    date_from: Some(*self.date_from()),
                    date_to: Some(*self.date_to()),
                    category: Some(cat_name_re),
                    account: Some(acct_name_re),
                    ..Default::default()
                };

                let filt_transactions = transaction_query.exec(db);
                let sum = sum_transactions(&filt_transactions);
                let cat_name = cat.name().to_string();
                let acct_name = acct.name().to_string();

                let val = match cat.parent_name(db) {
                    Some(parent_name) => ReviewSummary {
                        account_name: acct_name,
                        cat_name: parent_name.to_string(),
                        subcat_name: Some(cat_name),
                        n_transactions: filt_transactions.len(),
                        sum,
                    },
                    None => ReviewSummary {
                        account_name: acct_name,
                        cat_name,
                        subcat_name: None,
                        n_transactions: filt_transactions.len(),
                        sum,
                    },
                };

                val
            })
            .collect();

        // sort by account name, category name, then by subcategory name
        vals.sort_by(|a, b| {
            if a.account_name == b.account_name {
                if a.cat_name == b.cat_name {
                    a.subcat_name.cmp(&b.subcat_name)
                } else {
                    a.cat_name.cmp(&b.cat_name)
                }
            } else {
                a.account_name.cmp(&b.account_name)
            }
        });

        // filter out any categories with 0 transactions, if desired
        if self.excluded_none() {
            vals.iter()
                .filter(|v| v.n_transactions != 0)
                .cloned()
                .collect()
        } else {
            vals
        }
    }
}
