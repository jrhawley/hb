//! Query the budget in your HomeBank database.

use super::{FIRST_OF_NEXT_MONTH_STR, TODAY_FIRST_OF_MONTH_STR};
use crate::{transaction::{sum_transactions, transaction_date::HB_MIN_DATE}, Category, HomeBankDb, Query, QueryTransactions};

use chrono::{Local, NaiveDate};
use clap::Parser;
use regex::Regex;
use std::{fmt::Display, str::FromStr};

/// Query the budget in your HomeBank database.
#[derive(Debug, Parser)]
pub struct QueryBudget {
    /// Name of the category.
    #[clap(value_name = "regex")]
    pub(crate) name: Option<Regex>,

    /// Consider the budget from the month including this date.
    #[clap(
        short = 'd',
        long = "date-from",
        default_value = &TODAY_FIRST_OF_MONTH_STR,
        parse(try_from_str = NaiveDate::from_str),
        value_name = "date"
    )]
    pub(crate) date_from: NaiveDate,

    /// Consider the budget from the month up to and excluding this date.
    #[clap(
        short = 'D',
        long = "date-to",
        default_value = &FIRST_OF_NEXT_MONTH_STR,
        parse(try_from_str = NaiveDate::from_str),
        value_name = "date"
    )]
    pub(crate) date_to: NaiveDate,
}

impl QueryBudget {
    /// Create a new query for budgets
    pub fn new(name: Option<Regex>, date_from: NaiveDate, date_to: NaiveDate) -> Self {
        Self {
            name,
            date_from,
            date_to,
        }
    }

    /// Retrieve the regular expression for the `Category` name
    fn name(&self) -> &Option<Regex> {
        &self.name
    }

    /// Retrieve the earliest date that the budget is including
    fn date_from(&self) -> &NaiveDate {
        &self.date_from
    }

    /// Retrieve the latest date that the budget is including
    fn date_to(&self) -> &NaiveDate {
        &self.date_to
    }
}

#[derive(Debug)]
/// The sum of all [`Transaction`s][crate::transaction::transaction_struct::Transaction], as well as budget information, for a given [`Category`].
pub struct BudgetSummary {
    /// The [`Category`] name.
    pub name: String,

    /// The sub-[`Category`] name, if there is one.
    pub subname: Option<String>,

    /// The total sum of [`Transaction`s][crate::transaction::transaction_struct::Transaction] over the time span provided.
    progress: f32,

    /// How much room is allotted for this [`Category`] over the time span provided.
    allotment: Option<f32>,
}

impl BudgetSummary {
    /// Create a new budget summary
    pub fn new(name: &str, subname: Option<&str>, progress: f32, allotment: Option<f32>) -> Self {
        Self {
            name: name.to_string(),
            subname: subname.map(|val| val.to_string()),
            progress,
            allotment,
        }
    }

    /// Retrieve the name of the [`Category`] to which the budget applies
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Retrieve the name of the sub-[`Category`] to which the budget applies, if there is one
    pub fn subname(&self) -> &Option<String> {
        &self.subname
    }

    /// Retrieve the progress of the budget
    pub fn progress(&self) -> f32 {
        self.progress
    }

    /// Retrieve the progress of the budget, made positive, and rounded to the nearest integer
    pub fn progress_rounded(&self) -> u64 {
        self.progress.abs() as u64
    }

    /// Retrieve the progress of the budget
    ///
    /// This isn't stored as an explicit value anymore, but can be calculated dynamically.
    pub fn progress_frac(&self) -> Option<f32> {
        self.allotment.map(|val| self.progress / val)
    }

    /// Retrieve the allotment for the budget
    pub fn allotment(&self) -> Option<f32> {
        self.allotment
    }

    /// Retrieve the allotment for the budget, made positive, and rounded to the nearest integer
    pub fn allotment_rounded(&self) -> Option<u64> {
        self.allotment.map(|val| val.abs() as u64)
    }

    /// Helper function to determine if there is a budget or not
    pub fn has_allotment(&self) -> bool {
        self.allotment.is_some()
    }
}

impl Default for QueryBudget {
    fn default() -> Self {
        Self::new(None, *HB_MIN_DATE, Local::now().date_naive())
    }
}

impl Display for QueryBudget {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Query for QueryBudget {
    type T = BudgetSummary;

    fn exec(&self, db: &HomeBankDb) -> Vec<Self::T> {
        let mut filt_categories: Vec<Category> = db
            .categories()
            .values()
            // filter out categories that don't match the regex, if there is one
            .filter(|&cat| match self.name() {
                Some(re) => re.is_match(&cat.full_name(db)),
                None => true,
            })
            .cloned()
            .collect();

        filt_categories.sort_by_key(|a| a.full_name(db));

        let budget_spent: Vec<BudgetSummary> = filt_categories
            .iter()
            .map(|cat| {
                let cat_name_re = Regex::new(&cat.full_name(db)).unwrap();
                let transaction_query = QueryTransactions {
                    date_from: Some(*self.date_from()),
                    date_to: Some(*self.date_to()),
                    category: Some(cat_name_re),
                    ..Default::default()
                };

                let filt_transactions = transaction_query.exec(db);
                let sum = sum_transactions(&filt_transactions);
                let allotment = cat.budget_amount_over_interval(*self.date_from(), *self.date_to());

                if cat.is_child() {
                    BudgetSummary::new(
                        cat.parent_name(db).unwrap(),
                        Some(cat.name()),
                        sum,
                        allotment,
                    )
                } else {
                    BudgetSummary::new(cat.name(), None, sum, allotment)
                }
            })
            .collect();

        eprintln!("{}", self);
        budget_spent
    }
}
