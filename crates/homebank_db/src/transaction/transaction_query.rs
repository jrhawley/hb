//! Options for filtering [`Transaction`s][crate::transaction::transaction_struct::Transaction] from the [`HomeBankDb`].

use super::{TransactionStatus, TransactionType};
use crate::{HomeBankDb, PayMode, Query, Transaction};
use chrono::NaiveDate;
use clap::Parser;
use regex::Regex;
use std::{fmt::Display, str::FromStr};

/// Options for filtering [`Transaction`s][crate::transaction::transaction_struct::Transaction] from the [`HomeBankDb`].
#[derive(Debug, Parser)]
#[clap(
    name = "transactions",
    visible_alias = "t",
    about = "Query transactions"
)]
pub struct QueryTransactions {
    /// Include transactions starting from (and including) this date.
    #[clap(
        short = 'd',
        long = "date-from",
        parse(try_from_str = NaiveDate::from_str),
        value_name = "date"
    )]
    pub(crate) date_from: Option<NaiveDate>,

    /// Include transactions up to (and excluding) this date.
    #[clap(
        short = 'D',
        long = "date-to",
        parse(try_from_str = NaiveDate::from_str),
        value_name = "date"
    )]
    pub(crate) date_to: Option<NaiveDate>,

    /// Include transactions greater than (and including) this amount.
    #[clap(short = 'l', long = "amount-lower", value_name = "amount")]
    pub(crate) amount_from: Option<f32>,

    /// Include transactions less than (and excluding) this amount.
    #[clap(short = 'u', long = "amount-upper", value_name = "amount")]
    pub(crate) amount_to: Option<f32>,

    /// Include transactions with a certain status.
    #[clap(short = 's', long = "status", value_name = "status")]
    pub(crate) status: Option<Vec<TransactionStatus>>,

    /// Include transactions with categories that match the regular expression.
    #[clap(short = 'c', long = "category", value_name = "regex")]
    pub(crate) category: Option<Regex>,

    /// Include transactions involving payees that match the regular expression.
    #[clap(short = 'p', long = "payee", value_name = "regex")]
    pub(crate) payee: Option<Regex>,

    /// Include transactions involving accounts that match the regular expression.
    #[clap(short = 'a', long = "account", value_name = "regex")]
    pub(crate) account: Option<Regex>,

    /// Include transactions with a certain payment method.
    #[clap(short = 'M', long = "method", value_name = "method")]
    pub(crate) pay_mode: Option<Vec<PayMode>>,

    /// Include transactions whose memos match this regular expression.
    #[clap(short = 'm', long = "memo", value_name = "regex")]
    pub(crate) memo: Option<Regex>,

    /// Include transactions whose info fields match this regular expression.
    #[clap(short = 'i', long = "info", value_name = "regex")]
    pub(crate) info: Option<Regex>,

    /// Include transactions whose tags match this regular expression.
    #[clap(short = 't', long = "tag", value_name = "regex")]
    pub(crate) tags: Option<Regex>,

    /// Include `Expense`, `Income`, or `Transfer` transactions.
    #[clap(short = 'T', long = "type", value_name = "type")]
    pub(crate) transaction_type: Option<Vec<TransactionType>>,
}

impl QueryTransactions {
    /// Create a trivial query that filters out nothing.
    pub fn empty() -> Self {
        Self {
            date_from: None,
            date_to: None,
            amount_from: None,
            amount_to: None,
            status: None,
            category: None,
            payee: None,
            account: None,
            pay_mode: None,
            memo: None,
            tags: None,
            info: None,
            transaction_type: None,
        }
    }

    /// Select the lower bound date for querying
    pub fn date_from(&self) -> &Option<NaiveDate> {
        &self.date_from
    }

    /// Select the upper bound date for querying
    pub fn date_to(&self) -> &Option<NaiveDate> {
        &self.date_to
    }

    /// Select the lower bound amount for querying
    pub fn amount_from(&self) -> &Option<f32> {
        &self.amount_from
    }

    /// Select the upper bound amount for querying
    pub fn amount_to(&self) -> &Option<f32> {
        &self.amount_to
    }

    /// Select the status(es) for including in the query
    pub fn status(&self) -> &Option<Vec<TransactionStatus>> {
        &self.status
    }

    /// Select the category regex for including in the query
    pub fn category(&self) -> &Option<Regex> {
        &self.category
    }

    /// Select the payee regex for including in the query
    pub fn payee(&self) -> &Option<Regex> {
        &self.payee
    }

    /// Select the account regex for including in the query
    pub fn account(&self) -> &Option<Regex> {
        &self.account
    }

    /// Select the payment method(s) for including in the query
    pub fn pay_mode(&self) -> &Option<Vec<PayMode>> {
        &self.pay_mode
    }

    /// Select the memo regex for including in the query
    pub fn memo(&self) -> &Option<Regex> {
        &self.memo
    }

    /// Select the info regex for including in the query
    pub fn info(&self) -> &Option<Regex> {
        &self.info
    }

    /// Select the tags regex for including in the query
    pub fn tags(&self) -> &Option<Regex> {
        &self.tags
    }

    /// Select the transaction type for including in the query
    pub fn ttype(&self) -> &Option<Vec<TransactionType>> {
        &self.transaction_type
    }

    /// Filter out dates occurring before the query date
    pub fn filter_date_from(&self, tr: &Transaction) -> bool {
        match self.date_from() {
            Some(d) => tr.date() >= d,
            None => true,
        }
    }

    /// Filter out dates occurring after the query date
    pub fn filter_date_to(&self, tr: &Transaction) -> bool {
        match self.date_to() {
            Some(d) => tr.date() < d,
            None => true,
        }
    }

    /// Filter out amounts below the query amount lower bound
    pub fn filter_amount_from(&self, tr: &Transaction) -> bool {
        match self.amount_from() {
            Some(a) => tr.total() >= a,
            None => true,
        }
    }

    /// Filter out amounts above the query amount upper
    pub fn filter_amount_to(&self, tr: &Transaction) -> bool {
        match self.amount_to() {
            Some(a) => tr.total() < a,
            None => true,
        }
    }

    /// Filter out by status
    pub fn filter_status(&self, tr: &Transaction) -> bool {
        match self.status() {
            Some(v) => v.contains(tr.status()),
            None => true,
        }
    }

    /// Filter by payee names
    pub fn filter_payee(&self, tr: &Transaction, db: &HomeBankDb) -> bool {
        match (self.payee(), tr.payee_name(db)) {
            // if there is a regex and there is a category name
            (Some(re), Some(t_payee_name)) => re.is_match(&t_payee_name),
            // if there is a regex but no category
            (Some(_), None) => false,
            // if there is no regex
            (None, _) => true,
        }
    }

    /// Filer by account name
    pub fn filter_account(&self, tr: &Transaction, db: &HomeBankDb) -> bool {
        match (self.account(), tr.account_name(db)) {
            // if there is a regex and there is an account name
            (Some(re), Some(tr_account_name)) => re.is_match(&tr_account_name),
            // if there is a regex but no account
            (Some(_), None) => false,
            // if there is no regex
            (None, _) => true,
        }
    }

    /// Filter by payment method
    pub fn filter_paymode(&self, tr: &Transaction) -> bool {
        match self.pay_mode() {
            Some(v) => v.contains(tr.pay_mode()),
            None => true,
        }
    }

    /// Filter by `TransactionType`
    pub fn filter_ttype(&self, tr: &Transaction) -> bool {
        match self.ttype() {
            Some(v) => v
                .iter()
                // check transaction types without explicitly checking the values
                .any(|queried_type| queried_type.is_similar_to(tr.ttype())),
            None => true,
        }
    }

    /// Filter by tags
    pub fn filter_tags(&self, tr: &Transaction) -> bool {
        match (self.tags(), tr.tags()) {
            (Some(re), Some(tags)) => {
                // combine all the tags back into a single string to perform a single regex match
                // this avoids performing the costly match multiple times
                let combined_tr_tags = tags.join(",");
                re.is_match(&combined_tr_tags)
            }
            (Some(_), None) => false,
            (None, _) => true,
        }
    }

    /// Filter by memo
    pub fn filter_memo(&self, tr: &Transaction) -> bool {
        match (self.memo(), tr.memo()) {
            (Some(re), Some(memo)) => re.is_match(memo),
            (Some(_), None) => false,
            (None, _) => true,
        }
    }

    /// Filter by info
    pub fn filter_info(&self, tr: &Transaction) -> bool {
        match (self.info(), tr.info()) {
            (Some(re), Some(info)) => re.is_match(info),
            (Some(_), None) => false,
            (None, _) => true,
        }
    }

    /// Filter map the `Transaction` by the `Category`
    pub fn filter_category(&self, tr: &Transaction, db: &HomeBankDb) -> Option<Transaction> {
        match self.category() {
            Some(re) => {
                let matching_idx: Vec<usize> = tr
                    .category_names(db)
                    .iter()
                    .enumerate()
                    .filter_map(|(i, cat)| match cat {
                        Some(u) => {
                            if re.is_match(u) {
                                Some(i)
                            } else {
                                None
                            }
                        }
                        None => None,
                    })
                    .collect();

                // return the subset of the `Transaction` that matches the category query
                tr.subset(&matching_idx)
            }
            None => Some(tr.clone()),
        }
    }
}

impl Default for QueryTransactions {
    fn default() -> Self {
        Self::empty()
    }
}

impl Display for QueryTransactions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut msg = String::from("Transactions:");

        if let Some(date) = self.date_from() {
            msg.push_str(&format!(" on or after {date};"));
        }
        if let Some(date) = self.date_to() {
            msg.push_str(&format!(" before {date};"));
        }
        if let Some(val) = self.amount_from() {
            msg.push_str(&format!(" greater than ${val};"));
        }
        if let Some(val) = self.amount_to() {
            msg.push_str(&format!(" less than ${val};"));
        }
        if let Some(status) = self.status() {
            msg.push_str(&format!(" status one of '{status:?}';"));
        }
        if let Some(payee) = self.payee() {
            msg.push_str(&format!(" payee matching '{payee}';"));
        }
        if let Some(acct_re) = self.account() {
            msg.push_str(&format!(" account name matching '{acct_re}';"));
        }
        if let Some(pay_mode) = self.pay_mode() {
            msg.push_str(&format!(" payee one of '{pay_mode:?}';"));
        }
        if let Some(ttype) = self.ttype() {
            msg.push_str(&format!(" transaction type one of '{ttype:?}';"));
        }
        if let Some(tags_re) = self.tags() {
            msg.push_str(&format!(" tags matching '{tags_re}';"));
        }
        if let Some(memo_re) = self.memo() {
            msg.push_str(&format!(" memo matching '{memo_re}';"));
        }
        if let Some(info_re) = self.info() {
            msg.push_str(&format!(" info matching '{info_re}';"));
        }
        if let Some(cat_re) = self.category() {
            msg.push_str(&format!(" category matching '{cat_re}';"));
        }

        write!(f, "{}", msg)
    }
}

impl Query for QueryTransactions {
    type T = Transaction;

    fn exec(&self, db: &HomeBankDb) -> Vec<Self::T> {
        let filt_transactions: Vec<Transaction> = db
            .transactions()
            .iter()
            .filter(|&tr| self.filter_date_from(tr))
            .filter(|&tr| self.filter_date_to(tr))
            .filter(|&tr| self.filter_amount_from(tr))
            .filter(|&tr| self.filter_amount_to(tr))
            .filter(|&tr| self.filter_status(tr))
            .filter(|&tr| self.filter_payee(tr, db))
            // TODO: Check that the accounts are being filtered properly
            // if there is a transfer, you have to do extra work to see what hsould and shouldn't be included
            .filter(|&tr| self.filter_account(tr, db))
            .filter(|&tr| self.filter_paymode(tr))
            .filter(|&tr| self.filter_ttype(tr))
            .filter(|&tr| self.filter_tags(tr))
            .filter(|&tr| self.filter_memo(tr))
            .filter(|&tr| self.filter_info(tr))
            .filter_map(|tr| self.filter_category(tr, db))
            .collect();

        filt_transactions
    }
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;
    use regex::Regex;

    use crate::{
        category::CategoryBudget,
        transaction::{SimpleTransaction, SplitTransaction, TransactionComplexity},
        Account, AccountType, Category, HomeBankDb, PayMode, Query, QueryTransactions, Transaction,
        TransactionStatus, TransactionType,
    };

    #[test]
    fn split_transactions_filtered_properly() {
        let mut db = HomeBankDb::empty();
        let acct = Account {
            key: 0,
            flags: 0,
            pos: 1,
            atype: AccountType::Chequing,
            currency_idx: 0,
            name: String::from("Chequing"),
            bank_name: String::from("Bank"),
            initial_amount: 100.0,
            minimum_amount: 0.0,
            maximum_amount: 10_000.0,
            notes: String::new(),
            group_idx: None,
            reconciled_date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
        };
        let fun_cat = Category {
            key: 0,
            flags: 0,
            name: String::from("Fun Money"),
            budget: CategoryBudget::empty(),
            parent_key: None,
        };
        let serious_cat = Category {
            key: 1,
            flags: 0,
            name: String::from("Serious Money"),
            budget: CategoryBudget::empty(),
            parent_key: None,
        };
        let split_tr = Transaction {
            date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
            amount: 100.0,
            account: 0,
            pay_mode: PayMode::CreditCard,
            status: TransactionStatus::Reconciled,
            flags: None,
            payee: None,
            memo: None,
            info: None,
            tags: None,
            transaction_type: TransactionType::Expense,
            complexity: TransactionComplexity::Split(SplitTransaction::new(
                2,
                &[Some(0), Some(1)],
                &[90.0, 10.0],
                &[None, None],
            )),
        };
        let fun_tr = Transaction {
            date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
            amount: 90.0,
            account: 0,
            pay_mode: PayMode::CreditCard,
            status: TransactionStatus::Reconciled,
            flags: None,
            payee: None,
            memo: None,
            info: None,
            tags: None,
            transaction_type: TransactionType::Expense,
            complexity: TransactionComplexity::Split(SplitTransaction::new(
                1,
                &[Some(0)],
                &[90.0],
                &[None],
            )),
        };
        let serious_tr = Transaction {
            date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
            amount: 10.0,
            account: 0,
            pay_mode: PayMode::CreditCard,
            status: TransactionStatus::Reconciled,
            flags: None,
            payee: None,
            memo: None,
            info: None,
            tags: None,
            transaction_type: TransactionType::Expense,
            complexity: TransactionComplexity::Split(SplitTransaction::new(
                1,
                &[Some(1)],
                &[10.0],
                &[None],
            )),
        };

        let _ = db.add_account(&acct);
        let _ = db.add_category(&fun_cat);
        let _ = db.add_category(&serious_cat);
        let _ = db.add_transaction(&split_tr);

        let q_none = QueryTransactions::default();
        let q_fun = QueryTransactions {
            category: Some(Regex::new("Fun Money").unwrap()),
            ..Default::default()
        };
        let q_serious = QueryTransactions {
            category: Some(Regex::new("Serious Money").unwrap()),
            ..Default::default()
        };

        assert_eq!(q_none.exec(&db), vec![split_tr]);
        assert_eq!(q_fun.exec(&db), vec![fun_tr]);
        assert_eq!(q_serious.exec(&db), vec![serious_tr]);
    }

    #[test]
    fn accounts_properly_filtered_in_similar_transactions() {
        let mut db = HomeBankDb::empty();
        let acct1 = Account {
            key: 0,
            flags: 0,
            pos: 1,
            atype: AccountType::Chequing,
            currency_idx: 0,
            name: String::from("Chequing"),
            bank_name: String::from("Bank"),
            initial_amount: 100.0,
            minimum_amount: 0.0,
            maximum_amount: 10_000.0,
            notes: String::new(),
            group_idx: None,
            reconciled_date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
        };
        let acct2 = Account {
            key: 1,
            flags: 0,
            pos: 1,
            atype: AccountType::Asset,
            currency_idx: 0,
            name: String::from("Investment"),
            bank_name: String::from("Bank"),
            initial_amount: 0.0,
            minimum_amount: 0.0,
            maximum_amount: 10_000.0,
            notes: String::new(),
            group_idx: None,
            reconciled_date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
        };
        let cat = Category {
            key: 0,
            flags: 0,
            name: String::from("Investment"),
            budget: CategoryBudget::empty(),
            parent_key: None,
        };
        let withdrawal_tr = Transaction {
            date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
            amount: 100.0,
            account: 0,
            pay_mode: PayMode::ElectronicPayment,
            status: TransactionStatus::Reconciled,
            flags: None,
            payee: None,
            memo: None,
            info: None,
            tags: None,
            transaction_type: TransactionType::Expense,
            complexity: TransactionComplexity::Simple(SimpleTransaction::new(Some(0), 100.0, None)),
        };
        let deposit_tr = Transaction {
            date: NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
            amount: 100.0,
            account: 1,
            pay_mode: PayMode::ElectronicPayment,
            status: TransactionStatus::Reconciled,
            flags: None,
            payee: None,
            memo: None,
            info: None,
            tags: None,
            transaction_type: TransactionType::Income,
            complexity: TransactionComplexity::Simple(SimpleTransaction::new(Some(0), 100.0, None)),
        };

        let _ = db.add_account(&acct1);
        let _ = db.add_account(&acct2);
        let _ = db.add_category(&cat);
        let _ = db.add_transaction(&withdrawal_tr);
        let _ = db.add_transaction(&deposit_tr);

        let q_none = QueryTransactions::default();
        let q_acct1 = QueryTransactions {
            category: Some(Regex::new("Investment").unwrap()),
            account: Some(Regex::new("Chequing").unwrap()),
            ..Default::default()
        };
        let q_acct2 = QueryTransactions {
            category: Some(Regex::new("Investment").unwrap()),
            account: Some(Regex::new("Investment").unwrap()),
            ..Default::default()
        };

        assert_eq!(
            q_none.exec(&db),
            vec![withdrawal_tr.clone(), deposit_tr.clone()]
        );
        assert!(q_acct1.filter_account(&withdrawal_tr, &db));
        assert!(!q_acct1.filter_account(&deposit_tr, &db));
        assert!(q_acct2.filter_account(&deposit_tr, &db));
        assert!(!q_acct2.filter_account(&withdrawal_tr, &db));
        assert_eq!(q_acct1.exec(&db), vec![withdrawal_tr]);
        assert_eq!(q_acct2.exec(&db), vec![deposit_tr]);
    }
}
