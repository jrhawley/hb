{ pkgs, craneLib, ... }: let
    inherit (pkgs) lib stdenv;

    pname = "hb";
    src = ./.;

    commonArgs = {
      inherit src pname;
      strictDeps = true;

      buildInputs = [
        # Add optional inputs here
      ] ++ lib.optionals stdenv.isDarwin [
        # Add darwin specific inputs here
        pkgs.libiconv
      ];
    };

    #   craneLibLLvmTools = craneLib.overrideToolchain (
    #     fenix.packages.${system}.complete.withComponents [
    #       "cargo"
    #       "llvm-tools"
    #       "rustc"
    #     ]
    #   );

    cargoArtifacts = craneLib.buildDepsOnly commonArgs;
in craneLib.buildPackage (
  commonArgs // {
    inherit cargoArtifacts;
    cargoExtraArgs = "--bin hb";
  }
)
