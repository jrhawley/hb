# Changelog

## [0.3.4] - 2025-01-06

### Changed

- `hb review` now creates a tab-separated table of transactions sums per account
  - Previously, this summed across every account in the database, but that didn't allow for reviews of an individual account
  - This new behaviour is a breaking change in the CLI, but the original functionality can be recreated by some light processing from the user

## [0.3.3] - 2025-01-04

### Added

- Parse and store the `memo` of a `Transaction`
  - Previously, this was ignored, but is properly parsed now

## [0.3.2] - 2024-06-24

### Added

- Configuration files now support relative paths to the HomeBank XHB files
  - Paths are resolved relative to the configuration file itself
  - i.e. If the configuration file is `~/.config/hb/config.toml` and contains `path = ../homebank/homebank.xhb`, the path is resolved to `~/.config/homebank/homebank.xhb`
- New `add_transaction()`, `add_category()`, and `add_account()` methods for `HomeBankDb`
  - This allows for some initial programmatic write access to the database
  - Currently, this is only used internally for testing, but the methods themselves are public
- Minor doc changes

### Fixed

- Transaction queries were not properly filtering accounts by name, before and has been fixed
  - This bug also caused issues using the `hb sum` subcommand, which has been subsequently fixed
  
### Removed

- `Transaction::new()` has been removed
  - The function was only used once, internally, and mismatched the constructor-style creation that is used throughout the code base
  - `Transaction`s should be constructed in this manner (i.e. `Transaction { amount: 100.0, ..Default::default() }`)

## [0.3.1] - 2024-03-03

### Changed

- `budget` subcommand no longer displays progress bars, but outputs a tab-separated summary of a budget, similar to `review`

## [0.3.0] - 2022-12-19

### Added

- `review` subcommand to sum all transactions across each category.

## [0.2.0] - 2022-05-05

### Added

- Adding broader support for `Category` budgets in the `homebank_db` crate
- Display budget progress with the `budget` subcommand
  - Renders progress bars of transactions within a category and compares that against budgets set for those categories
  - Can search for categories by their name or consider specific time intervals

## [0.1.1] - 2022-04-22

### Added

- Ability to query by parent + sub-category
  - Parent categories are separated by their sub-categories with a `:`
  - It was previously impossible to distinguish between sub-categories with the same name, e.g. `Hello:There` and `WhoGoes:There`.
  - Now, you can include the entire parent + sub-category name in the query.

## [0.1.0] - 2022-04-14

- Initial release
